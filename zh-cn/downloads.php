<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - 下载中心</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
        .page main .info h4 {
            text-indent: 2rem;
        }
        .page main .info li {
            font-size: 14px;
            line-height: 30px;
            text-indent: 2rem;
            font-weight: bold;
        }
        .downloads-btns {
            margin: 15px 0;
        }
        .downloads-btns a {
            display: inline-block;
            width: 200px;height: 36px;line-height: 36px;
            border-radius: 5px;
            background: #c01a28;
            color: #fff;
            font-size: 12px;
            text-align: center;
            margin: 15px 25px;
        }

        @media (max-width: 768px) {
            .downloads-btns {
                display: flex;
                justify-content: space-around;
            }
            .downloads-btns a {
                width: 150px;
            }
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>下载中心</h1>
            <div class="info">
                <h4>KOALA交易平台</h4>
                <p>MetaTrader4 是用于外汇，金属和期货市场交易的先进交易平台</p>
                <p>MetaTrader4 是世界上最受欢迎的交易客户终端，它提供了交易外汇，金属，指数和商品的必要功能。</p>
                <figure>
                    <img src="assets/img/page_download.png" alt="">
                </figure>
                <p>MetaTrader 的操作界面包括一个主菜单，个性化工具栏，“市场报价”窗口，导航器，金融工具窗口，终端窗口。</p>
                <p>Metatrader 提供30种内置的技术分析指标帮助您分析多种金融工具价格。</p>
                <ul>
                    <li>实时交易和高效执行速度</li>
                    <li>自动交易使用的智能交易系统</li>
                    <li>一个平台可交易外汇、指数和差价合约</li>
                    <li>市场开放任何时段访问全球在线交易市场</li>
                    <li>适用於个人电脑（PC、Mac）、平板电脑和移动设备</li>
                </ul>
                <div class="downloads-btns">
                    <a href="https://download.mql5.com/cdn/web/10737/mt4/koalaforex4setup.exe">下载MT4</a>
                    <a href="../static/Koala_bin_setup.exe">下载期权插件</a>
                </div>
                
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>