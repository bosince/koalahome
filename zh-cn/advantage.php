<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - KOALA优势</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>KOALA优势</h1>
            <div class="info">
                <p>当您选择KOALA，您选择的是一家能从您身为交易者的良好发展中获得既得利益的期权撮合商。让客户的时间与投资效益最大化对我们来说至关重要，这也是为什么我们以提供最佳的人文关怀和客户服务为荣。</p>
                <p>我们的目标是成为行业领头者。随着金融市场不断进化，我们的策略和方法同样需要改进。我们做的每一个决定，都以交易者的需求为出发点。</p>
                <p>在评估客户申请的时候，KOALA也可能会从公共的资源里收集未来客户的信息。客户信息将会严格地按照英国隐私权法案中的国家隐私条例处理。居住在英国的客户在任何时候提出要求，都可根据以下所述的国家隐私条例取得KOALA保存的关于他或她的个人信息。</p>
                <h4>创新的交易环境</h4>
                <p>KOALA从交易者的角度出发，细节处处都为交易者所着想。<br>KOALA可以提供与银行、共同基金类似的流动性与扩展性。<br>KOALA采用了最新技术构建，使交易更安全更稳定。<br>超快速开户、畅通银联出入金、快速指令执行，解决客户后顾之忧。</p>
                <h4>通过KOALA进行交易</h4>
                <p>KOALA拥有严守条例和承诺的专业文化，深受其用户好评与尊敬。<br>KOALA的所有信托基金都被单独存放在银行授信账户。</p>
                <p>KOALA一直以来秉承客户优先原则，致力于为客户提供最优秀的服务<br>KOALA能够为客户提供个性化服务。我们具有丰富交易经验的专业团队可以为您在撮合交易中提供您所关心和需要的帮助。</p>
                <p>现代技术，移动应用，辅助工具等使得交易更加快速便捷<br>为了给客户提供持续有效的交易帮助，KOALA一直在应用新技术并不断开发新工具和应用。<br>KOALA在您的交易业中提供一切所需的帮助。</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>