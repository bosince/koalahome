<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - 财经新闻</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>财经新闻</h1>
            <div class="info">
                <div style='padding:60px 0;background-color: #fff'>
                    <iframe id="iframe"  height="1080"  width="100%"  frameborder="0"  scrolling="yes"  src="https://rili-d.jin10.com/open.php"></iframe>
                </div>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>