<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - 保密协议</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>保密协议</h1>
            <div class="info">
                <h4>客户信息</h4>
                <p>当客户向KOALA询问有关产品和服务，浏览KOALA的网站，或提交开户申请时，客户有可能要向KOALA提供个人信息。</p>
                <p>KOALA将保留所有帐户的交易和活动记录，包括交易合约的细节以及追加保证金通知。在合约关系期间，任何被客户使用过的产品和服务信息都将被记录存档。</p>
                <p>在评估客户申请的时候，KOALA也可能会从公共的资源里收集未来客户的信息。客户信息将会严格地按照英国隐私权法案中的国家隐私条例处理。居住在英国的客户在任何时候提出要求，都可根据以下所述的国家隐私条例取得KOALA保存的关于他或她的个人信息。</p>
                <h4>个人讯息的使用</h4>
                <p>KOALA在开户申请表中要求客户填写的信息是用于审核客户是否有足够的知识和经验在KOALA进行场外金融衍生产品的交易。这些信息以及在帐户开立期间由KOALA收集和保存的信息，都是为了让客户能获得他们帐户信息，保证金要求和交易活动的最新情况。</p>
                <p>客户在进入我们的网站或在填写我们的帐户申请表时所被要求提供的信息，是为了让我们能更好地向客户提供最适合他们投资风险承受力的产品和服务。KOALA将采取所有合理措施来保护客户的个人信息不会被滥用，遗失，未经授权进入，修改或披露。</p>
                <h4>电话会谈</h4>
                <p>KOALA也可能会对客户和KOALA授权代表之间的电话会谈录音。该录音，或录音的副本，可能会用于解决任何客户的争端。由KOALA提供的客户电话会谈的录音或录音副本，会由KOALA自行决定是否删除。</p>
                <h4>网站</h4>
                <p>KOALA会收集关于我们网站访客的统计讯息，如访客的数目，浏览的网页，进行过的交易类型，在线的时间以及下载过的文件。这些讯息会被用于评估和提高我们网站的品质。除了统计讯息，我们不会通过我们的网站收集任何个人信息，除非是必须向我们提供的信息。</p>
                <h4>更新个人讯息</h4>
                <p>如果客户档案中的个人信息有任何更改，KOALA要求要立即向本公司作出通知。这样能确保KOALA可以及时通知客户有关他们的帐户，保证金要求以及交易活动的信息。客户可以随时要求我们更正过时的或不正确的个人信息。如果我们对客户所提供信息的准确性有所质疑，客户可以要求我们对该信息附上一份声明并注明您认为该信息是不正确或不完整的。</p>
                <h4>客户同意</h4>
                <p>客户进入KOALA的网站就代表了客户同意他们所提供的个人信息被收集、维护、使用和披露。</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>