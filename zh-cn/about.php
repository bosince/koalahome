<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - 关于我们</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/about.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="container">
            <div class="banner-info visible-lg magictime openDownLeftRetourn">
                <h1>考拉集团</h1>
                <p>考拉集团（KOALA GROUP）总部位于美国，考拉是一家面向全球发展的国际顶尖撮合式金融服务平台，长期以来集中所有资源，全力投身于金融行业中。考拉集团旨在为全球客户提供多层次、多元化、全方位的金融服务，打造世界级的一流交易平台。</p>

                <p>考拉集团高起点、注重口碑信誉，在英国、澳大利亚、美国等多地设立办事处。我们提供24小时外汇、黄金、白银、原油等多元化环球金融产品在线交易，为客户提供方便快捷的电子金融服务。我们通过连接众多的报价供应商为客户提供顶级机构级别的流动性接入，以确保最及时的浮动点差数据更新和可交易的深度，最直观的表现方式为执行最优浮动点差、享受超低交易成本。</p>
            </div>
            <div class="banner-list-wrap visible-lg">
                <div class="banner-list-item banner-list-item1 magictime perspectiveUpRetourn">
                    <h4>愿景</h4>
                    <p>以科技创造价值，做全球醒来的专业化金融平台</p>
                </div>
                <div class="banner-list-item banner-list-item2 magictime perspectiveUpRetourn">
                    <h4>目标</h4>
                    <p>打造安全诚信的全球交易平台，为客户提供稳定的交易环境和更好的服务</p>
                </div>
                <div class="banner-list-item banner-list-item3 magictime perspectiveUpRetourn">
                    <h4>价值观</h4>
                    <p>双赢，构建信誉升级</p>
                </div>
                <div class="banner-list-item banner-list-item4 magictime perspectiveUpRetourn">
                    <h4>使命</h4>
                    <p>建立客户对外汇的信任和信息，为客户创造价值</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-banner-info hidden-lg">
        <div class="m-banner-info1">
            <h1>考拉集团</h1>
            <p>考拉集团（KOALA GROUP）总部位于美国，考拉是一家面向全球发展的国际顶尖撮合式金融服务平台，长期以来集中所有资源，全力投身于金融行业中。考拉集团旨在为全球客户提供多层次、多元化、全方位的金融服务，打造世界级的一流交易平台。</p>

            <p>考拉集团高起点、注重口碑信誉，在英国、澳大利亚、美国等多地设立办事处。我们提供24小时外汇、黄金、白银、原油等多元化环球金融产品在线交易，为客户提供方便快捷的电子金融服务。我们通过连接众多的报价供应商为客户提供顶级机构级别的流动性接入，以确保最及时的浮动点差数据更新和可交易的深度，最直观的表现方式为执行最优浮动点差、享受超低交易成本。</p>
        </div>
        <ul class="m-banner-info2">
            <li>
                <h4>愿景</h4>
                <p>以科技创造价值，做全球醒来的专业化金融平台</p>
            </li>
            <li>
                <h4>目标</h4>
                <p>打造安全诚信的全球交易平台，为客户提供稳定的交易环境和更好的服务</p>
            </li>
            <li>
                <h4>价值观</h4>
                <p>双赢，构建信誉升级</p>
            </li>
            <li>
                <h4>使命</h4>
                <p>建立客户对外汇的信任和信息，为客户创造价值</p>
            </li>
        </ul>
    </section>

    <section class="supervise">
        <div class="container">
            <div class="supervise-info">
                <h1>NFA 监管</h1>
                <p>KOALA受美国全国期货协会(National Futures Association—NFA)授权并监管，监管牌照号：0509259。我们严格遵守NFA的资金规定，公司与客户资金严格分离。</p>
            </div>
            <div class="supervise-img hidden-xs">
                <img src="assets/img/about/nfa.png" alt="">
            </div>
        </div>
    </section>

    <div class="safe">
        <div class="safe-img hidden-xs">
        </div>
        <div class="safe-info">
            <h1>资金安全</h1>
            <p>考拉集团严格执行客户资金隔离制度，客户的专属资金账户与公司资金完全分离，只能于客户授权下以交易目的调拨资金，任何其他人或机构均无法从客户的交易账户内提取资金，以严格确保客户资金安全。同时，我们选择花旗银行，汇丰银行，恒生银行等作为客户资金托管银行，并拟定一系列内部监管保障措施，保证投资者资金安全。</p>
        </div>
    </div>

    <div class="plan">
        <div class="plan-info">
            <h1>客户保障计划</h1>
            <p>考拉集团严格恪守美国NFA的监管条例，为客户提供专业、透明的金融服务。作为全球最严苛的金融监管机构之一，NFA让所有经纪商监管记录以电子可查询的形式出现，增强了经纪商对公众的透明度。NFA监督经纪商对交易员和投资商的行为规范，确保所有金融服务提供商的合规合法，同时使交易员和投资商免受欺骗，让客户可以于稳健的金融市场进行投资活动。</p>
        </div>
        <div class="plan-space hidden-xs hidden-sm"></div>
    </div>
    <div class="partner">
        <ul>
            <li><img src="assets/img/about/partner01.jpg" alt=""></li>
            <li><img src="assets/img/about/partner02.jpg" alt=""></li>
            <li><img src="assets/img/about/partner03.jpg" alt=""></li>
            <li><img src="assets/img/about/partner04.jpg" alt=""></li>
            <li><img src="assets/img/about/partner05.jpg" alt=""></li>
        </ul>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>