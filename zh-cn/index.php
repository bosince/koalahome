<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="https://cdn.bootcss.com/hover.css/2.1.1/css/hover-min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">
    
</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="banner-info">
            <h1>撮合交易平台</h1>
            <h2>自主撮合 + 公开透明</h2>
            <p>交易订单在投资者之间撮合成交，交易所不参与交易环节，而产品报价由国际市场行情报价决定。</p>
        </div>

        <div class="circle hidden-xs hidden-sm">
            <figure>
                <img src="assets/img/stock_exchange.png" alt="">
            </figure>
        </div>
    </section>

    <section class="product">
        <h1>KOALA 产品</h1>
        <p>更多的交易机会，更好的投资方案</p>
        <div id="featured-area" class="visible-lg"> 
            <ul> 
                <li>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="info">
                        外汇
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>外汇</h3>
                        <p>外汇交易市场是全球最大的金融产品市场，也是世界上最活跃与最具流动性的金融产品，每日有超过5万亿美金的资本流动。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>

                </li>
                <li>
                    <img src="assets/img/pro2.jpg" alt="" />
                    <div class="info">
                        指数
                    </div>
                    <div class="active-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>指数</h3>
                        <p>股指CFD是以股票指数为标的物，对股票投资者来说是非常有效的套期保值避险工具。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>
                </li>  
                <li>
                    <img src="assets/img/pro3.jpg" alt="" />
                    <div class="info">
                        期权
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>期权</h3>
                        <p>交易订单在投资者之间撮合成交，交易所不参与交易环节，而产品报价由国际市场行情报价决定。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>
                </li>  
            </ul> 
        </div>
        <ul class="m-product-wrap hidden-lg"> 
            <li>
                <div>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>外汇</h3>
                        <p>外汇交易市场是全球最大的金融产品市场，也是世界上最活跃与最具流动性的金融产品，每日有超过5万亿美金的资本流动。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>
                </div>
                
            </li>
            <li>
                <div>
                    <img src="assets/img/pro2.jpg" alt="" />

                    <div class="m-product-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>指数</h3>
                        <p>股指CFD是以股票指数为标的物，对股票投资者来说是非常有效的套期保值避险工具。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>
                </div>
            </li>  
            <li>
                <div>
                    <img src="assets/img/pro3.jpg" alt="" />

                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>期权</h3>
                        <p>交易订单在投资者之间撮合成交，交易所不参与交易环节，而产品报价由国际市场行情报价决定。</p>
                        <a href="https://my.koalafx.net/register">创建账户</a>
                    </div>
                </div>
            </li>  
        </ul> 
    </section>


    <section class="adv">
        <h1>我们的优势</h1>
        <div class="container advs-wrap">
            <div class="row hidden-xs">
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv01.png" alt="">
                        <h4>受严格监管的经纪商</h4>
                        <p class="hover-hidden">KOALA 集团 受美国 NFA 监管</p>
                        <p class="hover-show">KOALA 集团 受美国NFA(National Futures Association---NFA) 监管，监管号：0509259，作为全球最严苛的金融监管机构之一， NFA 代表整个外汇零售行业，负责维持秩序和教育投资大众，并不断加强监管。</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv02.png" alt="">
                        <h4>全球知名</h4>
                        <p class="hover-hidden">我们的客户来自超过196国家</p>
                        <p class="hover-show">我们的客户来自超过196国家，拥有超过30种员工语言。我们的管理团队曾到访超过120座全球各地的城市以了解客户和合作伙伴的需求。</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv03.png" alt="">
                        <h4>专注于客户</h4>
                        <p class="hover-hidden">客户至上是我们坚持的唯一准则</p>
                        <p class="hover-show">在KOALA，我们不在乎您的资金规模，客户至上是我们坚持的唯一准则，而并非资金、账户类型或投资规模多少。我们的所有客户都会接受到同质量的服务，同样的执行速度，同样的客户支持。</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv04.png" alt="">
                        <h4>交易商品的范围</h4>
                        <p class="hover-hidden">一个平台来交易多种商品</p>
                        <p class="hover-show">可以使用一个平台来交易多种商品，KOALA使交易变得更简单、有效。</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv05.png" alt="">
                        <h4>透明与公平</h4>
                        <p class="hover-hidden">所见即所得，没有任何隐藏条款</p>
                        <p class="hover-show">在KOALA ，所见即所得，没有任何隐藏条款。就是你所见的价格、执行与促销活动。我们所宣传的就是我们可以给到所有客户的，不论投资规模。</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item hvr-glow">
                        <img src="assets/img/adv06.png" alt="">
                        <h4>方便快捷</h4>
                        <p class="hover-hidden">以客户为中心的原则</p>
                        <p class="hover-show">我们所有的系统都是建立在以客户为中心的原则上。例如我们的开户程序、账户管理、出入金以及交易，对客户来说都是很容易操作的。</p>
                    </div>
                </div>
            </div>
            <div class="row m-adv-items visible-xs">
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv01.png" alt="">
                        <h4>受严格监管的经纪商</h4>
                        <p>KOALA Group 受美国NFA(National Futures Association---NFA) 监管，监管号：0509259，作为全球最严苛的金融监管机构之一， NFA 代表整个外汇零售行业，负责维持秩序和教育投资大众，并不断加强监管。</p>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv02.png" alt="">
                        <h4>全球知名</h4>
                        <p>我们的客户来自超过196国家，拥有超过30种员工语言。我们的管理团队曾到访超过120座全球各地的城市以了解客户和合作伙伴的需求。</p>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv03.png" alt="">
                        <h4>专注于客户</h4>
                        <p>在KOALA，我们不在乎您的资金规模，客户至上是我们坚持的唯一准则，而并非资金、账户类型或投资规模多少。我们的所有客户都会接受到同质量的服务，同样的执行速度，同样的客户支持。</p>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv04.png" alt="">
                        <h4>交易商品的范围</h4>
                        <p>可以使用一个平台来交易多种商品，KOALA使交易变得更简单、有效。</p>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv05.png" alt="">
                        <h4>透明与公平</h4>
                        <p>在KOALA ，所见即所得，没有任何隐藏条款。就是你所见的价格、执行与促销活动。我们所宣传的就是我们可以给到所有客户的，不论投资规模。</p>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="adv-item">
                        <img src="assets/img/adv06.png" alt="">
                        <h4>方便快捷</h4>
                        <p>我们所有的系统都是建立在以客户为中心的原则上。例如我们的开户程序、账户管理、出入金以及交易，对客户来说都是很容易操作的。</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="banner2">
        <div class="banner2-top">
            <h1>深度流动性，合作顶级投行</h1>
            <p>更优报价，急速成交，让你的交易更高效</p>
        </div>
        <div class="banner2-bottom">
            <p>KOALA 与 Citibank、BARCLAYS、MorganStanley、UBS 等各大国际投行合作，向全球交易者提供深度的流动性支持，直通式报价，帮助客户挖掘出更优的价格，交易更加高效稳定。</p>
        </div>
    </section>

    <section class="highly-active">
        <div class="container">
            <h1>科技助力，交易更简单高效</h1>
            <p>MetaTrader4 是用于外汇，金属和期货市场交易的先进交易平台。</p>
            <p>MetaTrader4 是世界上最受欢迎的交易客户终端，它提供了交易外汇，金属，指数和商品的必要功能。</p>
            <img src="assets/img/downApp.png" alt="">
            <p>MetaTrader4 也同样适用于其他设备，例如 IPad 和 IPhone </p>
            <div class="highly-active-btns">
                <a href="https://download.mql5.com/cdn/web/10737/mt4/koalaforex4setup.exe">下载MT4</a>
                <a href="../static/Koala_bin_setup.exe">下载期权插件</a>
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="container">
            <img src="assets/img/index_contact.png" class="hidden-xs" alt="">
            <div class="contact-info">
                <h1>KOALA - 人人都是投资高手</h1>
                <p>投资的秘诀，不是评估某一行业对社会的影响有多大，或它的发展前景有多好，而是一间公司有多强的竞争优势，这优势可以维持多久。产品和服务的优越性持久而深厚，才能给投资者带来优厚的回报。</p>
                <p>———— 沃伦·巴菲特</p>
            </div>
        </div>
    </section>


    <?php include 'footer.html' ?>
    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/jquery.roundabout.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/common.js"></script>


    <script>
        $(document).ready(function(){ 
            $('#featured-area ul').roundabout({
                duration: 800,
                easing: 'swing',
                autoplay: true,
                minOpacity: 1,
                minScale: 0.6
            },function(){
                $(".roundabout-moveable-item:last-child").find(".info").css({
                    left: '0',
                    right: 'auto'
                });
            });

            
            $("#featured-area").on('animationStart', function(event) {
                $(".roundabout-in-focus").find(".info").css({
                    left: '0',
                    right: 'auto'
                }).end().siblings('.roundabout-moveable-item').find(".info").css({
                    right: '0',
                    left: 'auto'
                });
            });
            
            
            $(".adv-item").hover(function() {
                $(this).find('.hover-hidden').slideUp(400).siblings('.hover-show').slideDown(400);
            }, function() {
                $(this).find('.hover-hidden').slideDown(400).siblings('.hover-show').slideUp(400);
            });

        });
    </script>
    
</body>
</html>