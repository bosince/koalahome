<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Terms and Conditions</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Terms and Conditions</h1>
            <div class="info">
              <p>Koala is a regulated forex and CFD broker. We provide an all-around professional online trading platform for all traders. We offer world class financial derivatives trading through our top tier technologies and services.</p>
              <p>When you trade Forex and CFDs, you need to understand their complexity and high risk. It does not suitable for all investors. It depends on whether you are suitable for trading foreign exchange on margin and CFDs, and you should not invest without knowing the risks of these transactions. If you have any questions, it is important for you to look for an objective and professional advice.The followings are some of risks of trading CFDs. The product and services guide contains more information about risks, so make sure you read it before you open an account.</p>

              <h4>1、Koala does not provide any personal advice</h4>
              <p>We only provide general product information. Therefore, before you apply for an account, you must consider your investment objectives, financial situation, and needs,as well as trading margin foreign exchange and CFD contain high-risk. We recommend that you read our Product Disclosure Statement and Financial Services Guide carefully, and consult your independent financial adviser, tax adviser, and other professional advisers.We cannot guarantee the results of your trading.</p>

              <h4>2、You are not trading the underlying asset</h4>
              <p>When you trade margin Forex and CFDs, your profits or losses depend on a rise or fall in the price of the underlying product. You need to know that you do not have any interest in the underlying forex, indices, and commodities. You are trading CFDs based on price fluctuation. There is no physical exchange or commodity trading. CFDs are financial derivative product.</p>

              <h4>3、3.Over-the-counter(OTC)Financial Derivatives</h4>
              <p>When you open a trade on our platform, you are in the OTC derivative contract, and orders cannot be transferred. This means that you are dealing directly with us, and these trades (positions) can only be closed with us. In other words, foreign exchange and CFD contracts are issued by Koala, not through any exchanges, such as the US Stock Exchange.Koala is your product issuer.</p>

              <h4>4、Leverage</h4>
              <p>When trading FX, you only need a small margin to open a position. For example, if you trade AUD/USD with a value of $10,000 and a margin ratio of 0.5%, then you only need $50 to open a position. However, your risk in the market is $10,000. If your position gained 10%, you will earn $1,000. If you lose 10% of your position, you lose $1,000. The profit or loss of your trading depends on the size of the positions you open. Koala’s leverage is provided by the liquidity banks, so Koala reserves the right to adjust leverage at our own discretion.</p>

              <h4>5、Market volatility</h4>
              <p>Execution prices are based on the prices provided by our liquidity banks. The price of your trading products may fluctuate rapidly due to financial market news. Any changes in prices and spreads will have a direct impact on your account funds and positions. Price fluctuations can lead to a common situation called gapping, which occurred when opening price hugely differ from its closing price. Gapping caused by an unexpected economic event or a market announcement, especially when such information occurs outside the trading hours. Therefore, you may not have the opportunity to open or close positions between the two prices. The platform will execute your order at the next closest market price. You must bear the risk of a price gap, and your loss may exceed your account's net worth, which may cause your account go into negative balance. You have the responsibility to avoid negative account balance. Koala reserves the right to take further actions to recover the negative amount.</p>

              <h4>6、Slippage</h4>
              <p>Slippage is also a risk that traders will face during trading. It is the result of rapid price fluctuations, the actual executed price of your trade differ from your pre-set execution price. Koala implements straight through processing(STP) model, and all the orders are executed at the price provided by our clearing banks, so traders must understand slippage can occur in foreign exchange trading. In addition, your pending orders (including pending order for new trades or pending order to close existing trades) price is for reference only, and the final traded price is the bank's actual execution price. You need to understand that in a fast-moving market, the actual result of a pending order may differ from your pre-set execution price.</p>

              <h4>7、Mandatory liquidation risk</h4>
              <p>At any time, the existing funds in your account must remain above the mandatory liquidation level (the margin level must be above 50%), otherwise your open positions will be closed. However, please do not rely entirely on the system's liquidation order. It is your responsibility to manage your positions, account balances and account equity on your trading platform. To prevent a mandatory liquidation, you should have sufficient funds in your account. Note that even if the money you deposited earlier was sufficient at that time, it could quickly become insufficient due to the fast movement in the market. In addition, in the case of rapid market fluctuations, the hedging trades (locked positions) may also trigger mandatory liquidation. Traders should only open appropriate positions based on their available fund.</p>

              <h4>8、Counterparty risk</h4>
              <p>At any time, the existing funds in your account must remain above the mandatory liquidation level (the margin level must be above 50%), otherwise your open positions will be closed. However, please do not rely entirely on the system's liquidation order. It is your responsibility to manage your positions, account balances and account equity on your trading platform. To prevent a mandatory liquidation, you should have sufficient funds in your account. Note that even if the money you deposited earlier was sufficient at that time, it could quickly become insufficient due to the fast movement in the market. In addition, in the case of rapid market fluctuations, the hedging trades (locked positions) may also trigger mandatory liquidation. Traders should only open appropriate positions based on their available fund.</p>

              <h4>9、Client capital risk</h4>
              <p>All customer funds are segregated. This means that all customers' deposits are kept in Client Trust Account, which separate from our own operating funds. However, in uncontrollable situations, your funds will still be at risk.</p>

              <h4>10、Technical risk and other circumstances that affect your trades</h4>
              <p>Some circumstances may prevent you from executing orders or from logging into our trading platform or instantly deposit funds into your account. This includes system errors or termination of supply, platform maintenance, network connectivity issues, or some third-party failures (such as network vendors,power companies and payment providers) that you or we cannot control. We have an emergency solution on these issues, but sometimes you may still not able to enter the trading platform or deposit funds electronically. These technical risks and contingencies pose a risk when you want to open or close positions.</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>