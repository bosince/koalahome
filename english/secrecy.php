<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Secrecy agreement</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Secrecy agreement</h1>
            <div class="info">
                <h4>User infomations</h4>
                <p>When a Client asks for information about Koala's products and services, accesses the Koala website, or submits an application to open an account with Koala, they may be providing Koala with personal information.</p>
                <p>Koala will maintain records of all transactions and activities on accounts with Koala, including details of contracts traded and margin calls made. During the course of a relationship with Koala, information about products and services provided utilised by the Client will be kept on record. When assessing a Client application, Koala may also collect information about the prospective Client from publicly available sources.</p>
                <p>Client personal information will be treated strictly in accordance with the National Privacy Principles in the Australian Privacy Act. An Australian resident customer may at any time, upon request, gain access to the information that the Koala holds about him or her in accordance with the National Privacy Principles as described below.</p>
                <h4>Use of Personal Information</h4>
                <p>The information requested in the Account Application to open an account is required by Koala to determine whether a prospective Client has enough knowledge and experience to trade in off-exchange, Over-the-Counter derivatives with Koala. That information, together with the information collected and maintained by Koala for duration of an account, is required to keep Clients informed in regards to their account status, margin obligations, and trading activities.</p>
                <p>The information requested by Koala when accessing our website or completing one of our Account Applications is to allow us to provide our Clients with information regarding the products and services offered by Koala that best suit their investment needs and risk appetite. Koala takes all reasonable steps to protect Client's personal information from misuse, loss, unauthorised access, modification or disclosure.</p>
                <h4>Telephone Conversations</h4>
                <p>Koala may also record telephone conversations between the Client and Koala's authorised representatives. Such recordings, or transcripts from such recordings, may be used to resolve any Client dispute. Recordings or transcripts made by Koala of Client telephone conversations may be erased at Koala's discretion.</p>
                <h4>Website</h4>
                <p>Koala collects statistical information about visitors to our websites such as the number of visitors, pages viewed, types of transactions conducted, time online and documents downloaded. This information is used to evaluate and improve the performance of our websites. Other than statistical information, we do not collect any personal information through our website unless provided to us.</p>
                <h4>Updating Personal Information</h4>
                <p>Koala asks that Clients promptly notify the company of any changes to the personal information on file. This allows Koala to keep Clients informed regarding their accounts, margin obligations, and trading activities. You may ask us at any time to correct personal information held by Koala that is outdated or inaccurate. Should we disagree with you as to the accuracy of the information, you may request that we attach a statement to that information noting that you consider it inaccurate or incomplete.</p>
                <h4>Client Consent</h4>
                <p>By accessing Koala's website the Client consents to the collecting, maintaining, using and disclosing personal information provided.</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>