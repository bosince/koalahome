<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala Forex Limited</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="https://cdn.bootcss.com/hover.css/2.1.1/css/hover-min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">
    
</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="banner-info">
            <h1>Match trading platform</h1>
            <h2>Autonomous match + Transparency</h2>
            <p>Trading orders are arranged between investors, and the exchanges are not involved in the transaction, and the product quotation is determined by the quotation of international market.</p>
        </div>

        <div class="circle hidden-xs hidden-sm">
            <figure>
                <img src="assets/img/stock_exchange.png" alt="">
            </figure>
        </div>
    </section>

    <section class="product">
        <h1>KOALA Product</h1>
        <p>More trading opportunities, better investment options.</p>
        <div id="featured-area" class="visible-lg"> 
            <ul> 
                <li>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="info">
                        Foreign
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>Foreign</h3>
                        <p>The foreign exchange market is the world's largest market for financial products and the most active and liquid financial products in the world, with more than $5 trillion in capital flows daily.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>

                </li>
                <li>
                    <img src="assets/img/pro2.jpg" alt="" />
                    <div class="info">
                        Index
                    </div>
                    <div class="active-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>Index</h3>
                        <p>The CFD index is the subject matter of stock index, which is a very effective hedging instrument for stock investors.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>
                </li>  
                <li>
                    <img src="assets/img/pro3.jpg" alt="" />
                    <div class="info">
                        Options
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>Options</h3>
                        <p>Trading orders are brokered between investors, and the exchange does not participate in the transaction, and the product quotation is determined by the international market quotation.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>
                </li>  
            </ul> 
        </div>
        <ul class="m-product-wrap hidden-lg"> 
            <li>
                <div>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>Foreign</h3>
                        <p>The foreign exchange market is the world's largest market for financial products and the most active and liquid financial products in the world, with more than $5 trillion in capital flows daily.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>
                </div>
                
            </li>
            <li>
                <div>
                    <img src="assets/img/pro2.jpg" alt="" />

                    <div class="m-product-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>Index</h3>
                        <p>The CFD index is the subject matter of stock index, which is a very effective hedging instrument for stock investors.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>
                </div>
            </li>  
            <li>
                <div>
                    <img src="assets/img/pro3.jpg" alt="" />

                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>Options</h3>
                        <p>Trading orders are brokered between investors, and the exchange does not participate in the transaction, and the product quotation is determined by the international market quotation.</p>
                        <a href="https://my.koalafx.net/register">Create account</a>
                    </div>
                </div>
            </li>  
        </ul> 
    </section>


    <section class="adv">
        <h1>KOALA Advantage</h1>
        <div class="container advs-wrap">
            <div class="row">
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv01.png" alt="">
                        <h4>Licensed and Regulated Broker</h4>
                        <p class="hover-show">KOALA Group is under the supervision of the U.S.NFA(National Futures Association---NFA) NFA ID: 0509259 .As one of the toughest financial regulators in the world,The NFA, which represents the entire foreign exchange retail industry, responsible for maintaining financial order and guiding investors and constantly strengthening regulation standards.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv02.png" alt="">
                        <h4>Globally Renowned</h4>
                        <p class="hover-show">We have clients from over 196 countries and staff speaking over 30 languages. Our management has visited over 120 cities globally to understand clients’ and partners’ needs.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv03.png" alt="">
                        <h4>Focused on the Client</h4>
                        <p class="hover-show">Size does not matter. At Koala the client comes first regardless of net capital worth, account type or size of investment. All our clients receive the same quality services, the same execution, and the same level of support. Koala was founded on these values, and that will not change.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv04.png" alt="">
                        <h4>Range of Trading Instruments</h4>
                        <p class="hover-show">With a wide range of trading instruments available from a single multi asset platform Koala makes trading easier and efficient.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv05.png" alt="">
                        <h4>Transparent and Fair</h4>
                        <p class="hover-show">At Koala what you see is what you get, with no hidden terms. Be that pricing, execution or promotions. What we advertise is what we give our clients, regardless of the size of their investment.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv06.png" alt="">
                        <h4>Easy and Convenient</h4>
                        <p class="hover-show">All our systems are built and updated with the client in mind. Starting from our account opening procedure, to managing your account, depositing or withdrawing funds and finally trading, it’s all straightforward simple and easy to use for all our clients.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="banner2">
        <div class="banner2-top">
            <h1>Sufficient liquidity</h1>
            <p>Better offer, quick deal, make your trades more efficient.</p>
        </div>
        <div class="banner2-bottom">
            <p>KOALA and Citibank, and BARCLAYS, MorganStanley, each big international investment Banks such as UBS, offering global traders depth liquidity support, cut-through quotation, help customers to unearth better prices, more efficient and stable.</p>
        </div>
    </section>

    <section class="highly-active">
        <div class="container">
            <h1>Technology helps, trading is simpler and more efficient.</h1>
            <p>MetaTrader4 is an advanced trading platform for foreign exchange, metals and futures markets.</p>
            <p>MetaTrader4 is the world's most popular trading client terminal, which provides the necessary functions for trading foreign exchange, metals, indexes and commodities.</p>
            <img src="assets/img/downApp.png" alt="">
            <p>MetaTrader4 also applies to other devices, such as the IPad and IPhone.</p>
            <div class="highly-active-btns">
                <a href="https://download.mql5.com/cdn/web/10737/mt4/koalaforex4setup.exe">Downloads MT4</a>
                <a href="../static/Koala_bin_setup.exe">Downloads Options Plug-in</a>
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="container">
            <img src="assets/img/index_contact.png" class="hidden-xs" alt="">
            <div class="contact-info">
                <h1>KOALA - Everyone is a good investor.</h1>
                <p>The secret of investing is not to assess how much a particular industry affects a social exchange, or how good its prospects are, but how long a company has a competitive advantage and how long it can last.The advantages of products and services are long and profound, which can bring good returns to investors.</p>
                <p>———— Warren Edward Buffett</p>
            </div>
        </div>
    </section>


    <?php include 'footer.html' ?>
    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/jquery.roundabout.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/common.js"></script>


    <script>
        $(document).ready(function(){ 
            $('#featured-area ul').roundabout({
                duration: 800,
                easing: 'swing',
                autoplay: true,
                minOpacity: 1,
                minScale: 0.6
            },function(){
                $(".roundabout-moveable-item:last-child").find(".info").css({
                    left: '0',
                    right: 'auto'
                });
            });

            
            $("#featured-area").on('animationStart', function(event) {
                $(".roundabout-in-focus").find(".info").css({
                    left: '0',
                    right: 'auto'
                }).end().siblings('.roundabout-moveable-item').find(".info").css({
                    right: '0',
                    left: 'auto'
                });
            });
            
            
            // $(".adv-item").hover(function() {
            //     $(this).find('.hover-show').slideDown(400);
            // }, function() {
            //     $(this).find('.hover-show').slideUp(400);
            // });

        });
    </script>
    
</body>
</html>