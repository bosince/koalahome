<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Advantage</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>KOALA Advantage</h1>
            <div class="info">
                <p>When you choose Koala international, you choose an option broker who will benefit from your good development as a trader. It is important for us to maximize customer time and investment, which is why we are proud of providing the best humanistic care and customer service.</p>
                <p>Our goal is to become the industry leader. As the financial markets continue to evolve, our strategies and methods need to be improved as well. Every decision we make is based on the needs of the traders.</p>

                <h4>Innovative trading environment</h4>
                <p>Koala international from the perspective of traders, the details are everywhere for traders to consider.</p>
                <p>Koala international offers liquidity and scalability similar to banks and mutual funds. Koala uses the latest technology to make transactions safer and more stable. Ultra fast open an account, expedite UnionPay deposit and withdraw, express order execution, to solve customer worries.</p>
                <h4>Deal via Koala International</h4>
                <p>Koala international has a professional culture of adherence to regulations and promises and is well received and respected by its users.</p>
                <p>All trust funds of Koala international are separately deposited in bank credit accounts.</p>

                <h4>Koala international has been adhering to the customer first principle, is committed to providing customers with the best service</h4>
                <p>Koala international is able to provide personalized service to its customers. Our professional team with rich trading experience can provide you with the help you need and need in your matchmaking business.</p>
                <h4>Modern technology, mobile applications, auxiliary tools and so on, making transactions faster and more convenient</h4>
                <p>In order to provide continued and effective transaction assistance to customers, Koala international has been applying new technologies and developing new tools and applications. Koala international offers all the help you need in your trading business.</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>