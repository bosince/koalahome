<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - About</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/about.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="container">
            <div class="banner-info visible-lg magictime openDownLeftRetourn">
                <h1>KOALA Group</h1>
                <p>(KOALA KOALA GROUP GROUP), based in the United States, the KOALA is a global development oriented international top set type financial services platform, long focus all resources, fully engaged in the financial sector.Koala group aims to provide multi-level, diversified and all-round financial services for global customers and build a world-class trading platform.</p>

                <p>Koala group has a high starting point and a reputation for reputation. It has offices in the UK, Australia and the United States.We provide 24 hours of foreign exchange, gold, silver, crude oil and other diversified global financial products online trading, to provide customers with convenient and fast electronic financial services.We by connecting a number of suppliers to provide liquidity level of the top institutions access, in order to ensure the most timely the depth of the floating point differential data update and tradable, the most intuitionistic expression to perform optimal floating point difference, enjoy low transaction costs.</p>
            </div>
            <div class="banner-list-wrap visible-lg">
                <div class="banner-list-item banner-list-item1 magictime perspectiveUpRetourn">
                    <h4>Vision</h4>
                    <p>To create value by technology and become a professional financial platform to wake up in the world.</p>
                </div>
                <div class="banner-list-item banner-list-item2 magictime perspectiveUpRetourn">
                    <h4>Target</h4>
                    <p>We will build a global trading platform for security and integrity, and provide customers with stable trading environment and better services.</p>
                </div>
                <div class="banner-list-item banner-list-item3 magictime perspectiveUpRetourn">
                    <h4>Values</h4>
                    <p>Win-win, build credibility upgrade.</p>
                </div>
                <div class="banner-list-item banner-list-item4 magictime perspectiveUpRetourn">
                    <h4>Mission</h4>
                    <p>Build trust and information about foreign exchange, create value for customers.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-banner-info hidden-lg">
        <div class="m-banner-info1">
            <h1>KOALA Group</h1>
            <p>(KOALA KOALA GROUP GROUP), based in the United States, the KOALA is a global development oriented international top set type financial services platform, long focus all resources, fully engaged in the financial sector.Koala group aims to provide multi-level, diversified and all-round financial services for global customers and build a world-class trading platform.</p>

            <p>Koala group has a high starting point and a reputation for reputation. It has offices in the UK, Australia and the United States.We provide 24 hours of foreign exchange, gold, silver, crude oil and other diversified global financial products online trading, to provide customers with convenient and fast electronic financial services.We by connecting a number of suppliers to provide liquidity level of the top institutions access, in order to ensure the most timely the depth of the floating point differential data update and tradable, the most intuitionistic expression to perform optimal floating point difference, enjoy low transaction costs.</p>
        </div>
        <ul class="m-banner-info2">
            <li>
                <h4>Vision</h4>
                <p>To create value by technology and become a professional financial platform to wake up in the world.</p>
            </li>
            <li>
                <h4>Target</h4>
                <p>We will build a global trading platform for security and integrity, and provide customers with stable trading environment and better services.</p>
            </li>
            <li>
                <h4>Values</h4>
                <p>Win-win, build credibility upgrade.</p>
            </li>
            <li>
                <h4>Mission</h4>
                <p>Build trust and information about foreign exchange, create value for customers.</p>
            </li>
        </ul>
    </section>

    <section class="supervise">
        <div class="container">
            <div class="supervise-info">
                <h1>The NFA regulations</h1>
                <p>KOALA is authorized and regulated by the National Futures Association (NFA), a regulatory license plate: 0509259. We strictly abide by the capital requirements of NFA, and the company is strictly separated from the customer funds.</p>
            </div>
            <div class="supervise-img hidden-xs">
                <img src="assets/img/about/nfa.png" alt="">
            </div>
        </div>
    </section>

    <div class="safe">
        <div class="safe-img hidden-xs">
        </div>
        <div class="safe-info">
            <h1>Money safe</h1>
            <p>Koala group strictly enforce client money segregation, customer's exclusive completely separate capital account and the company funds, only to customers under the authorization transaction purpose in allocating funds, any other people or institutions are unable to withdraw funds from within the customer's trading account, to strictly ensure the security of the client money. At the same time, we chose citibank, HSBC, hang seng bank and other clients as custodian Banks, and drew up a series of internal regulatory safeguards to ensure the safety of investors' funds.</p>
        </div>
    </div>

    <div class="plan">
        <div class="plan-info">
            <h1>Customer support scheme</h1>
            <p>Koala group strictly abides by the regulations of NFA and provides professional and transparent financial services to customers. The NFA, one of the world's toughest financial regulators, has allowed all brokers to record their records electronically, enhancing the transparency of the brokers' public. NFA supervise brokers for traders and investors behavior standards, to ensure that all financial services provider of legal compliance, at the same time make the traders and investors from fraud, can let the customer to the steady financial markets for their investment activities.</p>
        </div>
        <div class="plan-space hidden-xs hidden-sm"></div>
    </div>
    <div class="partner">
        <ul>
            <li><img src="assets/img/about/partner01.jpg" alt=""></li>
            <li><img src="assets/img/about/partner02.jpg" alt=""></li>
            <li><img src="assets/img/about/partner03.jpg" alt=""></li>
            <li><img src="assets/img/about/partner04.jpg" alt=""></li>
            <li><img src="assets/img/about/partner05.jpg" alt=""></li>
        </ul>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>