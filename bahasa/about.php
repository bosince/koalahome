<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Tentang kami</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/about.css">

</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="container">
            <div class="banner-info visible-lg magictime openDownLeftRetourn">
                <h1>KOALA Group</h1>
                <p>KOALA GROUP Berkantor pusat di Amerika Serikat, Koala adalah penyedia jasa keuangan bergengsi global terkemuka yang telah lama berfokus pada semua sumber daya dan mengabdikan dirinya pada industri keuangan. Koala Group bertujuan untuk menyediakan layanan keuangan multi level, diversifikasi dan komprehensif kepada pelanggan global, untuk menciptakan platform perdagangan kelas satu kelas dunia.</p>

                <p>Koala Group merupakan titik awal yang tinggi, memperhatikan reputasi word of mouth, di Inggris, Australia, Amerika Serikat dan banyak kantor lainnya. Kami menyediakan perdagangan valuta asing, emas, perak, minyak mentah 24 jam dan transaksi online produk terdiversifikasi global lainnya, menyediakan pelanggan dengan layanan keuangan elektronik yang nyaman dan efisien. Kami menyediakan akses likuiditas tingkat institusi tingkat atas kepada klien kami dengan menghubungkan berbagai penyedia penawaran untuk memastikan update data terapung yang paling mutakhir dan kedalaman yang dapat diperdagangkan, cara yang paling intuitif untuk melakukan penyebaran float yang optimal dan menikmati ultra-low Biaya transaksi.</p>
            </div>
            <div class="banner-list-wrap visible-lg">
                <div class="banner-list-item banner-list-item1 magictime perspectiveUpRetourn">
                    <h4>Visi</h4>
                    <p>Untuk menciptakan nilai melalui sains dan teknologi, dan menjadi platform keuangan global yang khusus terbangun</p>
                </div>
                <div class="banner-list-item banner-list-item2 magictime perspectiveUpRetourn">
                    <h4>Tujuan</h4>
                    <p>Buat platform perdagangan global yang aman dan jujur untuk menyediakan lingkungan perdagangan yang stabil dan layanan yang lebih baik kepada pelanggan</p>
                </div>
                <div class="banner-list-item banner-list-item3 magictime perspectiveUpRetourn">
                    <h4>Nilai</h4>
                    <p>Menang-menang, bangun upgrade kredibilitas</p>
                </div>
                <div class="banner-list-item banner-list-item4 magictime perspectiveUpRetourn">
                    <h4>Misi</h4>
                    <p>Menetapkan kepercayaan nasabah dan informasi valuta asing, untuk menciptakan nilai bagi nasabah</p>
                </div>
            </div>
        </div>
    </section>
    <section class="m-banner-info hidden-lg">
        <div class="m-banner-info1">
            <h1>KOALA Group</h1>
            <p>KOALA GROUP Berkantor pusat di Amerika Serikat, Koala adalah penyedia jasa keuangan bergengsi global terkemuka yang telah lama berfokus pada semua sumber daya dan mengabdikan dirinya pada industri keuangan. Koala Group bertujuan untuk menyediakan layanan keuangan multi level, diversifikasi dan komprehensif kepada pelanggan global, untuk menciptakan platform perdagangan kelas satu kelas dunia.</p>

            <p>Koala Group merupakan titik awal yang tinggi, memperhatikan reputasi word of mouth, di Inggris, Australia, Amerika Serikat dan banyak kantor lainnya. Kami menyediakan perdagangan valuta asing, emas, perak, minyak mentah 24 jam dan transaksi online produk terdiversifikasi global lainnya, menyediakan pelanggan dengan layanan keuangan elektronik yang nyaman dan efisien. Kami menyediakan akses likuiditas tingkat institusi tingkat atas kepada klien kami dengan menghubungkan berbagai penyedia penawaran untuk memastikan update data terapung yang paling mutakhir dan kedalaman yang dapat diperdagangkan, cara yang paling intuitif untuk melakukan penyebaran float yang optimal dan menikmati ultra-low Biaya transaksi.</p>
        </div>
        <ul class="m-banner-info2">
            <li>
                <h4>Visi</h4>
                <p>Untuk menciptakan nilai melalui sains dan teknologi, dan menjadi platform keuangan global yang khusus terbangun</p>
            </li>
            <li>
                <h4>Tujuan</h4>
                <p>Buat platform perdagangan global yang aman dan jujur untuk menyediakan lingkungan perdagangan yang stabil dan layanan yang lebih baik kepada pelanggan</p>
            </li>
            <li>
                <h4>Nilai</h4>
                <p>Menang-menang, bangun upgrade kredibilitas</p>
            </li>
            <li>
                <h4>Misi</h4>
                <p>Menetapkan kepercayaan nasabah dan informasi valuta asing, untuk menciptakan nilai bagi nasabah</p>
            </li>
        </ul>
    </section>

    <section class="supervise">
        <div class="container">
            <div class="supervise-info">
                <h1>Peraturan NFA</h1>
                <p>KOALA diberi wewenang dan diatur oleh National Futures Association-NFA dengan nomor lisensi 0509259. Kami benar-benar mematuhi persyaratan pendanaan NFA, dana perusahaan dan nasabah dipisahkan secara ketat.</p>
            </div>
            <div class="supervise-img hidden-xs">
                <img src="assets/img/about/nfa.png" alt="">
            </div>
        </div>
    </section>

    <div class="safe">
        <div class="safe-img hidden-xs">
        </div>
        <div class="safe-info">
            <h1>Keamanan finansial</h1>
            <p>Koala Group menerapkan sistem isolasi dana nasabah secara ketat, rekening dana eksklusif nasabah dan dana perusahaan yang benar-benar terpisah, hanya di bawah wewenang klien untuk mentransfer dana untuk tujuan perdagangan, orang atau institusi lain tidak dapat menarik dana dari akun perdagangan pelanggan ke Ketat amannya keamanan dana nasabah. Pada saat yang sama, kami memilih Citibank, HSBC dan Hang Seng Bank sebagai bank kustodian untuk dana klien dan merumuskan serangkaian safeguards peraturan internal untuk memastikan keamanan dana investor.</p>
        </div>
    </div>

    <div class="plan">
        <div class="plan-info">
            <h1>Rencana Perlindungan Pelanggan</h1>
            <p>Koala Group secara ketat mematuhi peraturan peraturan NFA Amerika Serikat, untuk menyediakan layanan keuangan profesional dan transparan bagi pelanggan. Sebagai salah satu regulator keuangan yang paling ketat di dunia, NFA telah memungkinkan semua catatan peraturan broker muncul dalam format yang dapat ditelusuri secara elektronik, meningkatkan transparansi broker ke publik. NFA mengawasi kode etik broker untuk para pedagang dan investor, memastikan kepatuhan oleh semua penyedia jasa keuangan, dan melindungi pedagang dan investor dari kecurangan, yang memungkinkan mereka berinvestasi di pasar keuangan yang sehat.</p>
        </div>
        <div class="plan-space hidden-xs hidden-sm"></div>
    </div>
    <div class="partner">
        <ul>
            <li><img src="assets/img/about/partner01.jpg" alt=""></li>
            <li><img src="assets/img/about/partner02.jpg" alt=""></li>
            <li><img src="assets/img/about/partner03.jpg" alt=""></li>
            <li><img src="assets/img/about/partner04.jpg" alt=""></li>
            <li><img src="assets/img/about/partner05.jpg" alt=""></li>
        </ul>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>