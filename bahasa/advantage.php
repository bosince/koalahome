<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Keunggulan KOALA</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Keunggulan KOALA</h1>
            <div class="info">
                <p>Bila Anda memilih KOALA, Anda memilih broker pilihan yang mendapatkan kepentingan pribadi dari perkembangan Anda yang baik sebagai pedagang. Memaksimalkan waktu dan manfaat investasi klien kami sangat penting bagi kami, oleh karena itu kami bangga memberikan layanan dan layanan pelanggan manusiawi terbaik.</p>
                <p>Tujuan kami adalah menjadi pemimpin industri. Seiring pasar keuangan terus berkembang, strategi dan metode kita perlu ditingkatkan juga Setiap keputusan yang kita buat didasarkan pada kebutuhan trader.</p>
                <p>KOALA juga dapat mengumpulkan informasi tentang calon klien dari sumber publik saat menilai aplikasi klien.Informasi pelanggan akan ditangani secara ketat sesuai dengan Privacy Act di Inggris Privacy Act. Klien yang tinggal di Inggris, atas permintaan mereka setiap saat, dapat memperoleh informasi pribadi KOALA tentang dia berdasarkan undang-undang privasi nasional yang dijelaskan di bawah ini.</p>
                <h4>Lingkungan perdagangan yang inovatif</h4>
                <p>KOALA Dari sudut pandang pedagang, detail semuanya untuk trader.KOALA dapat memberikan likuiditas dan skalabilitas yang sama dengan bank, reksadana. KOALA menggabungkan teknologi terbaru untuk membuat transaksi lebih aman dan stabil. Akun ultra cepat, pembayaran akses UnionPay yang tidak terganggu, penerapan perintah yang cepat untuk mengatasi kekhawatiran pelanggan. </p>
                <h4>Deal dengan KOALA</h4>
                <p>KOALA memiliki budaya profesional yang ketat mematuhi peraturan dan komitmen, dipuji dan dihormati oleh penggunanya. Semua dana perwalian KOALA disetorkan secara terpisah ke rekening kredit bank. </p>
                <p>KOALA telah berpegang pada prinsip prioritas pelanggan, berkomitmen untuk memberikan layanan terbaik kepada pelanggan KOALA mampu menawarkan layanan personal kepada kliennya. Tim trading trading profesional berpengalaman kami dapat membantu Anda menyediakan layanan yang Anda minati dan butuhkan dalam kesepakatan Anda. </p>
                <p>Teknologi modern, aplikasi mobile, alat bantu dan lebih membuat perdagangan lebih cepat dan mudah, KOALA telah menerapkan teknologi baru dan terus mengembangkan alat dan aplikasi baru untuk memberikan bantuan transaksional yang berkesinambungan dan efektif kepada kliennya. KOALA menyediakan semua bantuan yang Anda butuhkan dalam bisnis perdagangan Anda. </p>
     
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>