<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Perjanjian kerahasiaan</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Perjanjian kerahasiaan</h1>
            <div class="info">
                <h4>Informasi pelanggan</h4>
                <p>Saat pelanggan meminta KOALA tentang produk dan layanan, melihat-lihat situs web KOALA, atau mengajukan aplikasi pembukaan akun, pelanggan mungkin diminta untuk memberikan KOALA dengan informasi pribadi.</p>
                <p>KOALA akan menyimpan catatan semua transaksi dan aktivitas rekening, termasuk rincian kontrak transaksi dan pemberitahuan margin tambahan. KOALA akan menyimpan catatan semua transaksi dan aktivitas rekening, termasuk rincian kontrak transaksi dan pemberitahuan margin tambahan.</p>
                <p>KOALA juga dapat mengumpulkan informasi tentang calon klien dari sumber publik saat menilai aplikasi klien. Informasi pelanggan akan ditangani secara ketat sesuai dengan Privacy Act di Inggris Privacy Act. Klien yang tinggal di Inggris, atas permintaan mereka setiap saat, dapat memperoleh informasi pribadi KOALA tentang dia berdasarkan undang-undang privasi nasional yang dijelaskan di bawah ini.</p>
                <h4>Penggunaan informasi pribadi</h4>
                <p>Informasi yang dibutuhkan KOALA dalam formulir aplikasi pembukaan rekening digunakan untuk memverifikasi bahwa klien memiliki pengetahuan dan pengalaman yang cukup untuk memperdagangkan produk derivatif OTC di KOALA.Informasi ini, serta informasi yang dikumpulkan dan dipelihara oleh KOALA selama periode pembukaan rekening, dimaksudkan untuk memberikan gambaran terkini tentang informasi akun, persyaratan margin dan aktivitas perdagangan mereka kepada pelanggan. </p>
                <p>Informasi yang diminta pelanggan untuk memasuki situs web kami atau mengisi formulir aplikasi kami adalah dengan memungkinkan kami memberikan produk dan layanan terbaik kepada pelanggan kami yang paling sesuai dengan toleransi risiko investasinya. KOALA akan mengambil semua langkah yang wajar untuk melindungi informasi pribadi Anda dari penyalahgunaan, kehilangan, akses tidak sah, modifikasi atau pengungkapan. </p>
                <h4>Percakapan telepon</h4>
                <p>KOALA juga dapat merekam percakapan telepon antara klien dan perwakilan resmi KOALA. Rekaman, atau salinan rekaman, dapat digunakan untuk menyelesaikan perselisihan klien. Salinan rekaman atau rekaman percakapan telepon pelanggan yang disediakan oleh KOALA akan sesuai dengan kebijakan KOALA apakah akan menghapusnya atau tidak. </p>
                <h4>Situs web</h4>
                <p>KOALA mengumpulkan informasi statistik tentang pengunjung ke situs kami, seperti jumlah pengunjung, halaman yang dikunjungi, jenis transaksi yang dilakukan, waktu online, dan dokumen yang didownload. Pesan ini akan digunakan untuk mengevaluasi dan meningkatkan kualitas website kita. Kami tidak mengumpulkan informasi pribadi dari situs kami kecuali untuk informasi statistik kecuali jika kami perlu memberikan informasi kepada kami  </p>
                <h4>Perbarui informasi pribadi </h4>
                <p>Jika ada perubahan informasi pribadi dalam file pelanggan, KOALA meminta segera memberitahukan perusahaan kami. Ini memastikan bahwa KOALA dapat segera memberi tahu klien tentang akun mereka, persyaratan margin dan informasi aktivitas perdagangan. Pelanggan mungkin meminta kami untuk memperbaiki informasi pribadi yang sudah usang atau tidak benar kapan saja. Jika kami mempertanyakan keakuratan informasi yang diberikan oleh pelanggan, pelanggan dapat meminta agar kami melampirkan pernyataan ke informasi dan menunjukkan bahwa Anda yakin informasinya salah atau tidak lengkap. </p>
                <h4>Pelanggan setuju </h4>
                <p>Akses pelanggan ke situs KOALA mewakili persetujuan pelanggan terhadap pengumpulan, pemeliharaan, penggunaan dan pengungkapan informasi pribadi yang mereka berikan. </p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>