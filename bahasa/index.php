<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koala Forex Limited</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="//cdn.bootcss.com/animate.css/3.5.2/animate.min.css" >
    <link rel="stylesheet" href="https://cdn.bootcss.com/hover.css/2.1.1/css/hover-min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <link rel="stylesheet" href="assets/css/index.css">
    
</head>
<body>
    <?php include 'header.html' ?>
    
    <section class="banner">
        <div class="banner-info">
            <!-- <h1>Match trading platform</h1> -->
            <h2>Platform transaksi pengiriman secara mandiri cocok + terbuka dan transparan</h2>
            <p>Pesanan dagang yang ditengahi antara kesepakatan investor, pertukaran tidak dilibatkan dalam transaksi, dan kutipan harga produk dari pasar internasional.</p>
        </div>

        <div class="circle hidden-xs hidden-sm">
            <figure>
                <img src="assets/img/stock_exchange.png" alt="">
            </figure>
        </div>
    </section>

    <section class="product">
        <h1>KOALA Produk</h1>
        <p>Peluang trading lebih banyak, program investasi yang lebih baik</p>
        <div id="featured-area" class="visible-lg"> 
            <ul> 
                <li>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="info">
                        Devisa
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>Devisa</h3>
                        <p>Pasar perdagangan valas adalah pasar produk keuangan terbesar di dunia, tapi juga produk keuangan paling aktif dan paling likuid di dunia, lebih dari 5 triliun dolar AS per hari mengalir.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>

                </li>
                <li>
                    <img src="assets/img/pro2.jpg" alt="" />
                    <div class="info">
                        Indeks
                    </div>
                    <div class="active-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>Indeks</h3>
                        <p>Indeks saham Indeks CFD didasarkan pada indeks saham, bagi investor saham instrumen lindung nilai lindung nilai sangat efektif.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>
                </li>  
                <li>
                    <img src="assets/img/pro3.jpg" alt="" />
                    <div class="info">
                        Pilihan
                    </div>
                    <div class="active-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>Pilihan</h3>
                        <p>Indeks saham Indeks CFD didasarkan pada indeks saham, bagi investor saham instrumen lindung nilai lindung nilai sangat efektif.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>
                </li>  
            </ul> 
        </div>
        <ul class="m-product-wrap hidden-lg"> 
            <li>
                <div>
                    <img src="assets/img/pro1.jpg" alt="" />
                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro1_icon.png" alt="">
                        <h3>Devisa</h3>
                        <p>Pasar perdagangan valas adalah pasar produk keuangan terbesar di dunia, tapi juga produk keuangan paling aktif dan paling likuid di dunia, lebih dari 5 triliun dolar AS per hari mengalir.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>
                </div>
                
            </li>
            <li>
                <div>
                    <img src="assets/img/pro2.jpg" alt="" />

                    <div class="m-product-info magictime swap ">
                        <img src="assets/img/pro2_icon.png" alt="">
                        <h3>Indeks</h3>
                        <p>Indeks saham Indeks CFD didasarkan pada indeks saham, bagi investor saham instrumen lindung nilai lindung nilai sangat efektif.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>
                </div>
            </li>  
            <li>
                <div>
                    <img src="assets/img/pro3.jpg" alt="" />

                    <div class="m-product-info magictime swap">
                        <img src="assets/img/pro3_icon.png" alt="">
                        <h3>Pilihan</h3>
                        <p>Indeks saham Indeks CFD didasarkan pada indeks saham, bagi investor saham instrumen lindung nilai lindung nilai sangat efektif.</p>
                        <a href="https://my.koalafx.net/register">Buat akun</a>
                    </div>
                </div>
            </li>  
        </ul> 
    </section>


    <section class="adv">
        <h1>Keuntungan kami</h1>
        <div class="container advs-wrap">
            <div class="row">
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv01.png" alt="">
                        <h4>Di bawah pengawasan ketat broker</h4>
                        <p class="hover-show">KOALA Group diatur oleh National Futures Association (NFA) di bawah pengawasan nomor 0509259. Sebagai salah satu badan pengatur keuangan yang paling ketat di dunia, NFA mewakili keseluruhan industri ritel valuta asing dan bertanggung jawab untuk menjaga ketertiban dan mendidik masyarakat. Meminta penguatan standar peraturan.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv02.png" alt="">
                        <h4>Dunia terkenal</h4>
                        <p class="hover-show">Klien kami berasal dari lebih dari 196 negara dan memiliki lebih dari 30 bahasa karyawan. Tim manajemen kami telah mengunjungi lebih dari 120 kota di seluruh dunia untuk memahami kebutuhan pelanggan dan mitra kami.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv03.png" alt="">
                        <h4>Fokus pada pelanggan</h4>
                        <p class="hover-show">Di KOALA, kami tidak peduli dengan ukuran uang Anda, supremasi pelanggan adalah satu-satunya kriteria yang kami patuhi, bukan ukuran dana, rekening atau investasi. Semua pelanggan kami akan menerima kualitas layanan yang sama, kecepatan eksekusi yang sama, dukungan pelanggan yang sama. Nilai yang dikuatkan KOALA dalam permulaannya tidak akan berubah.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv04.png" alt="">
                        <h4>Kisaran barang yang diperdagangkan</h4>
                        <p class="hover-show">Anda bisa menggunakan satu platform untuk menukar beberapa produk. KOALA membuat trading lebih mudah dan lebih efektif.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv05.png" alt="">
                        <h4>Transparan dan adil</h4>
                        <p class="hover-show">Di KOALA, WYSIWYG tidak memiliki persyaratan tersembunyi. Apakah harga yang anda lihat, kegiatan implementasi dan promosi. Apa yang kami promosikan adalah bahwa kami dapat memberikan kepada semua pelanggan, terlepas dari skala investasi.</p>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="adv-item">
                        <img src="assets/img/adv06.png" alt="">
                        <h4>Nyaman dan cepat</h4>
                        <p class="hover-show">Semua sistem kami dibangun berdasarkan prinsip customer-centric. Misalnya, prosedur pembukaan rekening, pengelolaan akun, deposit dan penarikan, serta transaksi, mudah dilakukan untuk klien kami.</p>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="banner2">
        <div class="banner2-top">
            <h1>Cukup likuiditasnya</h1>
            <p>Harga lebih baik, omset cepat, membuat trading anda lebih efisien</p>
        </div>
        <div class="banner2-bottom">
            <p>KOALA, bekerja sama dengan bank investasi internasional utama seperti Citibank, BARCLAYS, MorganStanley dan UBS, memberikan dukungan likuiditas yang mendalam dan kutipan langsung kepada pedagang global untuk membantu mereka menemukan harga yang lebih baik dan membuat transaksi lebih efisien dan stabil.</p>
        </div>
    </section>

    <section class="highly-active">
        <div class="container">
            <h1>Teknologi membantu, transaksi lebih sederhana dan efisien</h1>
            <p>MetaTrader4 (MT4) adalah terminal klien trading terpopuler di dunia dan menyediakan fungsionalitas yang diperlukan untuk perdagangan Forex, Logam, Indeks dan Komoditas.</p>
            <p>Antarmuka pengguna MetaTrader mencakup menu utama, toolbar hasil personalisasi, jendela "kutipan pasar", navigator, jendela instrumen keuangan dan jendela terminal. Metatrader menawarkan 30 indikator teknis yang ada untuk membantu Anda menganalisis harga berbagai instrumen.</p>
            <img src="assets/img/downApp.png" alt="">
            <p>MetaTrader4 juga berlaku untuk perangkat lain, seperti iPad dan iPhone</p>
            <div class="highly-active-btns">
                <a href="https://download.mql5.com/cdn/web/10737/mt4/koalaforex4setup.exe">Downloads MT4</a>
                <a href="../static/Koala_bin_setup.exe">Download Opsi Plugin</a>
            </div>
        </div>
    </section>

    <section class="contact">
        <div class="container">
            <img src="assets/img/index_contact.png" class="hidden-xs" alt="">
            <div class="contact-info">
                <h1>KOALA - Semua orang adalah seorang ahli investasi.</h1>
                <p>Rahasia berinvestasi bukan untuk menilai seberapa besar sebuah industri memiliki dampak pada pertukaran sosial, seberapa bagusnya, tapi seberapa kompetitif sebuah perusahaan dan berapa lama waktu yang dibutuhkan untuk bertahan. Keunggulan produk dan layanan yang tahan lama dan mendalam dapat membawa keuntungan yang sangat baik bagi investor.</p>
                <p>———— Warren Edward Buffett</p>
            </div>
        </div>
    </section>


    <?php include 'footer.html' ?>
    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/jquery.roundabout.js"></script>
    <script src="assets/js/jquery.easing.1.3.js"></script>
    <script src="assets/js/common.js"></script>


    <script>
        $(document).ready(function(){ 
            $('#featured-area ul').roundabout({
                duration: 800,
                easing: 'swing',
                autoplay: true,
                minOpacity: 1,
                minScale: 0.6
            },function(){
                $(".roundabout-moveable-item:last-child").find(".info").css({
                    left: '0',
                    right: 'auto'
                });
            });

            
            $("#featured-area").on('animationStart', function(event) {
                $(".roundabout-in-focus").find(".info").css({
                    left: '0',
                    right: 'auto'
                }).end().siblings('.roundabout-moveable-item').find(".info").css({
                    right: '0',
                    left: 'auto'
                });
            });
            
            
            // $(".adv-item").hover(function() {
            //     $(this).find('.hover-show').slideDown(400);
            // }, function() {
            //     $(this).find('.hover-show').slideUp(400);
            // });

        });
    </script>
    
</body>
</html>