<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Istilah hukum</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Istilah hukum</h1>
            <div class="info">
              <p>KOALA adalah broker valuta asing yang diatur dan menyediakan serangkaian penuh platform perdagangan online profesional untuk sebagian besar pengguna. Secara hukum, KOALA tidak melakukan intervensi atau menangani masalah pengelolaan aset. Sebaliknya, KOALA menyediakan layanan derivatif keuangan kepada kliennya melalui teknologi terdepan. Bila Anda memperdagangkan margin devisa dan CFD, Anda perlu memahami kompleksitas dan risiko tinggi. Ini tidak selalu berlaku untuk semua investor. Itu tergantung pada kesesuaian Anda untuk trading Forex Margin dan CFD, dan Anda seharusnya tidak melakukan investasi tanpa mengetahui risiko dari transaksi ini. Jika Anda memiliki pertanyaan, penting untuk menemukan saran objektif dan profesional. Berikut adalah beberapa risiko trading CFD. Panduan layanan produk berisi lebih banyak informasi tentang risikonya, jadi pastikan untuk membacanya sebelum membuka akun.</p>

              <h4>1.KOALA tidak memberikan saran pribadi apapun</h4>
              <p>Kami hanya menyediakan informasi produk secara umum. Oleh karena itu, sebelum mengajukan permohonan, Anda harus mempertimbangkan tujuan investasi, situasi keuangan dan kebutuhan Anda serta tingginya risiko perdagangan valuta asing dan CFD. Sebaiknya Anda membaca Pernyataan Pengungkapan Produk dan Panduan Jasa Keuangan secara hati-hati dan kemudian berkonsultasi dengan penasihat keuangan independen, penasihat pajak, dan penasihat profesional lainnya. Kami tidak dapat menjamin hasil Anda dalam transaksi.</p>

              <h4>2, Anda tidak menukar aset sebenarnya</h4>
              <p>Bila Anda memperdagangkan margin devisa dan CFD, keuntungan atau kerugian Anda berasal dari harga produk yang diperdagangkan naik atau turun. Anda perlu tahu bahwa Anda tidak membeli dan menjual komoditas seperti kontrak FX atau futures dunia nyata seperti emas dan perak. Anda menukar CFD berdasarkan fluktuasi harga tanpa pertukaran aktual atau perdagangan komoditas.,Milik derivatif keuangan.</p>

              <h4>3, derivatif keuangan OTC</h4>
              <p>Saat Anda melakukan perdagangan di platform perdagangan kami, Anda berada di tengah kontrak keuangan derivatif untuk over the counter (OTC) dan pesanan tidak dapat ditransfer. Ini berarti Anda berhubungan langsung dengan kami dan bahwa perdagangan (posisi) ini hanya bisa diakhiri dengan kami. Dengan kata lain, baik margin devisa maupun CFD diperdagangkan secara langsung dengan emiten kita, bukan melalui bursa, seperti American Stock Exchange. Karena itu, keuntungan Anda tidak ada kaitannya dengan keuntungan di pasar lain.</p>

              <h4>4, tuas</h4>
              <p>Bila Anda berada dalam perdagangan valuta asing, Anda hanya memerlukan sebagian kecil dari margin yang bisa dibuka. Misalnya, jika Anda membeli AUD / USD dengan nilai $ 10.000 dan rasio marginnya adalah 0,5%, Anda dapat membukanya seharga $ 50. Namun, risiko Anda di pasar adalah $ 10.000. Jika posisi anda 10% menguntungkan, anda akan mendapatkan $ 1000. Jika Anda kehilangan 10% dari posisi Anda, Anda kehilangan $ 1000. Keuntungan atau kerugian Anda bergantung pada ukuran posisi yang Anda buka. Leverage KOALA berasal dari bank kliring back-end, yang menawarkan keuntungan yang lebih baik untuk meningkatkan / menurunkan rasio margin untuk setiap produk, sehingga KOALA berhak untuk mengubah leverage sesuai kebutuhan bank untuk memberikan likuiditas dan pengendalian risiko.</p>

              <h4>5, volatilitas pasar</h4>
              <p>Eksekusi harga dan pesanan di platform trading kami didasarkan pada harga dan likuiditas di bursa dan pasar, serta data yang dikumpulkan oleh emiten kami. Harga dan penyebaran produk trading Anda mungkin berfluktuasi dengan cepat di pasar keuangan. Setiap perubahan pada penawaran dan spread produk akan berdampak langsung pada modal dan posisi akun Anda. Fluktuasi harga dapat menyebabkan fenomena umum yang disebut meroket karena perubahan harga mendadak dari harga saat ini ke tingkat yang lain. Fenomena ini mungkin disebabkan oleh kejadian ekonomi atau pengumuman pasar yang tidak terduga, terutama bila informasi semacam itu terjadi di luar jam perdagangan. Jadi, Anda mungkin tidak memiliki kesempatan untuk membuka atau menutup posisi di antara dua harga. Platform akan menjadi harga pasar terdekat terdekat untuk membantu Anda menangani. Anda harus menanggung risiko lonjakan harga, kerugian Anda mungkin melebihi aset bersih akun Anda, saldo akun menjadi negatif. Anda memiliki tanggung jawab dan kewajiban untuk mengisi kembali akun Anda secara negatif, dan KOALA berhak untuk meneruskan izin akun.</p>

              <h4>6, selip</h4>
              <p>Slippage juga merupakan risiko trader yang perlu ditangani dalam bertransaksi. Slippage disebabkan oleh fluktuasi harga yang cepat, harga transaksi aktual trader dan harga transaksi default tidak konsisten. KOALA mengambil akses langsung ke pasar Semua pesanan perdagangan perlu dikonfirmasi oleh bank kliring di belakang panggung dan kemudian dieksekusi.Oleh karena itu, para pedagang perlu mengetahui bahwa selip itu adalah fenomena objektif dalam transaksi. Selain itu, harga pending order (termasuk pembukaan pending order atau penutupan pending order) hanya untuk referensi, dan harga transaksi akhir didasarkan pada harga transaksi aktual bank. Yang perlu Anda ketahui adalah bahwa di pasar yang berfluktuasi dengan cepat, hasil sebenarnya dari pesanan yang tertunda mungkin berbeda dari hasil default Anda.</p>

              <h4>7, dipaksa likuidasi risiko</h4>
              <p>Setiap saat, dana yang ada di akun Anda harus disimpan di atas tingkat likuidasi wajib (rasio prabayar harus dipertahankan sebesar 50% atau lebih), atau posisi terbuka Anda akan menjadi satu per satu. Namun, tolong jangan mengandalkan sepenuhnya sistem secara otomatis tingkat yang kuat. Merupakan tanggung jawab Anda untuk mengelola posisi, saldo akun dan nilai akun Anda di Platform Perdagangan dengan cepat. Untuk mencegah likuidasi paksa, Anda harus menyimpan dana yang cukup di rekening Anda untuk mencegah potensi kerugian atau risiko dalam transaksi apa pun. Perhatikan bahwa meskipun dana yang Anda setorkan sebelumnya cukup memadai pada saat itu, karena perubahan pasar yang cepat, Anda mungkin cepat menjadi tidak memadai. Selain itu, dalam kasus fluktuasi yang cepat di pasar, bahkan jika daftar lindung nilai (lock-up) mungkin juga karena fluktuasi harga yang cepat yang menyebabkan penguncian daftar kuat. Ingatlah untuk membuka posisi yang sesuai berdasarkan tingkat dana di akun Anda. Jangan membuka posisi yang besar sehingga ruang pertahanan Anda tidak mencukupi.</p>

              <h4>8, risiko counterparty</h4>
              <p>Bila Anda membuka rekening di perusahaan kami dan membuka posisi margin, Anda membeli kontrak dagang, kami adalah penyedia kontrak dagang Anda. Dalam kasus ini, risiko transaksi adalah bahwa kita mungkin tidak dapat memenuhi kewajiban kontrak Anda dengan Anda di luar kendali kita. Ini mungkin karena kita atau mitra dagang kita sendiri (seperti institusi hedging untuk kita) default. Karena faktor yang tidak terkendali, kami tidak dapat menjamin kinerja kewajiban kontrak Anda. Selain itu, kami berhak menolak kinerja kewajiban kontraktual Anda, termasuk keuntungan perdagangan dan rabat agen Anda, jika metode trading dan strategi trading Anda bertentangan dengan piagam kontrol risiko kami.</p>

              <h4>9, risiko dana nasabah</h4>
              <p>Semua dana klien benar-benar terpisah. Ini berarti bahwa semua uang klien ditempatkan dalam posisi independen terpisah dari modal kerja kita. Dana simpanan nasabah ditempatkan di akun kepercayaan terpisah kami untuk perawatan dan operasi. Dengan situasi yang tidak terkendali, uang Anda akan tetap berisiko.</p>

              <h4>10, risiko teknis dan keadaan lain yang mempengaruhi trading anda</h4>
              <p>Dalam beberapa kasus, kami mungkin tidak dapat melakukan perintah, atau Anda tidak dapat mengakses platform trading kami, atau tidak akan dapat segera segera mewujudkan deposit. Ini termasuk kesalahan sistem server atau penghentian, fase pemeliharaan, masalah konektivitas jaringan, atau kesalahan pihak ketiga yang tidak dapat Anda kendalikan (misalnya, penyedia jaringan, perusahaan utilitas, atau pemberi pinjaman uang). Kami memiliki solusi darurat untuk masalah ini, namun jika terjadi keadaan darurat Anda mungkin masih belum dapat masuk ke platform perdagangan atau segera membuat deposit online. Risiko dan kontinjensi teknis ini menimbulkan risiko bila Anda ingin membuka atau menutup posisi dan Anda perlu menanggung akibat trading dari risiko tersebut.</p>
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>