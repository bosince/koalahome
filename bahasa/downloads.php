<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>KOALA - Pusat Download</title>
    <link rel="stylesheet" href="//cdn.bootcss.com/zui/1.8.0/css/zui.min.css">
    <link rel="stylesheet" href="//cdn.bootcss.com/magic/1.1.0/magic.min.css" >
    <link rel="stylesheet" href="assets/css/common.css">
    <style>
        .banner {
            background: url(assets/img/page_banner_bg1.jpg);
        }
        .page main .info h4 {
            text-indent: 2rem;
        }
        .page main .info li {
            font-size: 14px;
            line-height: 30px;
            text-indent: 2rem;
            font-weight: bold;
        }
        .downloads-btns {
            margin: 15px 0;
        }
        .downloads-btns a {
            display: inline-block;
            width: 200px;height: 36px;line-height: 36px;
            border-radius: 5px;
            background: #c01a28;
            color: #fff;
            font-size: 12px;
            text-align: center;
            margin: 15px 25px;
        }

        @media (max-width: 768px) {
            .downloads-btns {
                display: flex;
                justify-content: space-around;
            }
            .downloads-btns a {
                width: 180px;
            }
        }
    </style>
</head>
<body>
    <?php include 'header.html' ?>

    <div class="page">
        <section class="banner"></section>

        <main class="container magictime foolishIn">
            <h1>Pusat Download</h1>
            <div class="info">
                <h4>KOALA Trading platform</h4>
                <p>MetaTrader4 (MT4) adalah terminal klien trading terpopuler di dunia dan menyediakan fungsionalitas yang diperlukan untuk perdagangan Forex, Logam, Indeks dan Komoditas.</p>
                <p>Antarmuka pengguna MetaTrader mencakup menu utama, toolbar hasil personalisasi, jendela "kutipan pasar", navigator, jendela instrumen keuangan dan jendela terminal. Metatrader menawarkan 30 indikator teknis yang ada untuk membantu Anda menganalisis harga berbagai instrumen.</p>
                <figure>
                    <img src="assets/img/page_download.png" alt="">
                </figure>
                <!-- <p>MetaTrader's operating interface includes a main menu, personalized toolbar, "market quote" window, navigator, financial tool window, and terminal window.</p>
                <p>Metatrader provides 30 built-in technical analysis indicators to help you analyze the prices of various financial instruments.</p> -->
                <ul>
                    <li>Real-time trading dan kecepatan eksekusi yang efisien</li>
                    <li>Trading otomatis menggunakan sistem trading cerdas </li>
                    <li>Platform untuk perdagangan valuta asing, indeks dan CFD</li>
                    <li>Buka pasar kapan saja untuk mengakses pasar perdagangan online global</li>
                    <li>Cocok untuk PC (PC, Mac), tablet dan perangkat mobile</li>
                </ul>
                <div class="downloads-btns">
                    <a href="#">Downloads MT4</a>
                    <a href="#">Downloads plugin pilihan</a>
                </div>
                
            </div>
        </main>
    </div>

    <?php include 'footer.html' ?>

    <script src="//cdn.bootcss.com/zui/1.8.0/lib/jquery/jquery.js"></script>
    <script src="//cdn.bootcss.com/zui/1.8.0/js/zui.min.js"></script>
    <!-- bootstrap 二级菜单触发方式改为 hover -->
    <script src="//cdn.bootcss.com/bootstrap-hover-dropdown/2.0.10/bootstrap-hover-dropdown.min.js"></script>
    <!-- 页面往下滚动，导航条隐藏， 页面往上滚，导航条显示 -->
    <script src="//cdn.bootcss.com/headroom/0.9.4/headroom.min.js"></script>
    <script src="//cdn.bootcss.com/headroom/0.9.4/jQuery.headroom.min.js"></script>

    <script src="assets/js/common.js"></script>
</body>
</html>